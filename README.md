# seabreeze

Open source library to support Ocean Optics spectrometers

## Vendor documentation

https://oceanoptics.com/api/seabreeze/

## Vendor source

https://sourceforge.net/projects/seabreeze/

## Instructions

Minor modifications to the downloaded source are required to get the library to build in the ESS EPICS Environment (EEE).

1. Copy the following directories and files from ``seabreeze-x.y.z/SeaBreeze`` to ``m-epics-seabreeze/src/seabreeze``:

    * ``LICENCE``
    * ``include/``
    * ``os-support/``
    * ``src/``

        $ cd seabreeze-x.y.z/SeaBreeze

        $ cp -r LICENCE include os-support src ../../m-epics-seabreeze/src/seabreeze

2. Delete the following files and sub-directories:

    * ``src/os-support/windows``
    * ``src/native/network/windows``
    * ``src/native/rs232/windows``
    * ``src/native/system/windows``
    * ``src/native/usb/winusb``
    * ``src/native/usb/osx``
    * ``win.mk``

        $ cd m-epics-seabreeze/src/seabreeze

        $ rm -r src/os-support/windows src/native/network/windows src/native/rs232/windows src/native/system/windows src/native/usb/winusb src/native/usb/osx win.mk 

3. Delete all makefiles in the seabreeze source

        $ cd m-epics-seabreeze/src/seabreeze
    
        $ find . -name Makefile -exec rm {} \;


