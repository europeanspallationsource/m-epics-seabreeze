include $(EPICS_ENV_PATH)/module.Makefile

EXCLUDE_ARCHS = eldk

#STARTUPS = startup/qepro.cmd
#DOC      = doc
#OPIS     = opi

#INC += api/seabreezeapi/SeaBreezeAPI.h

#HEADERS_PREFIX = api/seabreezeapi/
#HEADERS += src/seabreeze/include/api/seabreezeapi/SeaBreezeAPI.h
#HEADERS_PREFIX = api/
#HEADERS += src/seabreeze/include/api/DllDecl.h

EXCLUDE_ARCHS = eldk
#DBDS = src/drvUSBQEProSupport.dbd

AUTO_DEPENDENCIES = NO
#USR_DEPENDENCIES += asyn,4.3+

#USR_CPPFLAGS += -I/vagrant/SeaBreeze/include -DLINUX
USR_CPPFLAGS += -I../../src/seabreeze/include


#USR_LDFLAGS += -L/vagrant/SeaBreeze/lib -lseabreeze
#SEABREEZE_DIR = ../../src/lib
#USR_LDFLAGS += -L$(SEABREEZE_DIR)
USR_LDFLAGS += -lusb

# This section is to force a copy of the full seabreeze include
# directory tree, retaining the structure.
#
# header_install is defined as a prerequisite of the main Makefile
# build task
#
# The copy is only performed on MAKELEVEL 2 as this is the location
# that has the correct path definition
#
# TODO: Convert this to a proper make rule, identifying the 
# header files in the source tree, then copying each one as required
#
build: header_install
header_install:
ifeq ($(MAKELEVEL),2)
	- $(CP) -r ../../src/seabreeze/include ${BUILD_PATH}/${EPICSVERSION}
endif
